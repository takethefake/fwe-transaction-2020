# Setup backend

-  cp ./packages/backend/.env.example ./packages/backend/.env

Adjust params to your needs

- docker-compose run backend npm install

- docker-compose up -d

- docker-compose exec backend npm run typeorm schema:sync
- docker-compose exec backend npm run fixtures
- docker-compose restart backend

