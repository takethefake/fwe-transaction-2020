import { Tag } from "./../../src/entity/Tag";

import "reflect-metadata";
// tslint:disable-next-line:no-var-requires
require("dotenv-safe").config();
import "jest";
import request from "supertest";
import { Helper } from "../helper";

describe("transaction", () => {
  const helper = new Helper();

  beforeAll(async () => {
    await helper.init();
  });

  afterAll(async () => {
    await helper.shutdown();
  });

  it("should be able to get all tags if authorized", async (done) => {
    const tag = new Tag();
    tag.label = "Testtag";

    const savedTag = await helper.getRepo(Tag).save(tag);

    const authToken = await helper.loginUser("dev@fwe.de");
    request(helper.app)
      .get("/api/tag")
      .set("Content-Type", "application/json")
      .set("Accept", "application/json")
      .set("Authorization", authToken)
      .expect(200)
      .end((err, res) => {
        if (err) throw err;
        expect(res.body.data.length).toBe(1);
        expect(res.body.data[0].id).toBe(savedTag.id);
        expect(res.body.data[0].label).toBe(savedTag.label);
        done();
      });
  });

  it("should be able to get now tags if unauthorized", async (done) => {
    const tag = new Tag();
    tag.label = "Testtag";

    await helper.getRepo(Tag).save(tag);

    request(helper.app)
      .get("/api/tag")
      .set("Content-Type", "application/json")
      .set("Accept", "application/json")
      .expect(401)
      .end((err) => {
        if (err) throw err;
        done();
      });
  });
});
