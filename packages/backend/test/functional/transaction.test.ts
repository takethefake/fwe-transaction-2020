import "reflect-metadata";
import { Transaction } from "./../../src/entity/Transaction";
import { User } from "./../../src/entity/User";
// tslint:disable-next-line:no-var-requires
require("dotenv-safe").config();
import "jest";
import request from "supertest";
import { Helper } from "../helper";

describe("transaction", () => {
  const helper = new Helper();

  beforeAll(async () => {
    await helper.init();
  });

  afterAll(async () => {
    await helper.shutdown();
  });

  it("should show all transactions", async (done) => {
    await helper.resetDatabase();
    await helper.loadFixtures();
    const transaction = new Transaction();
    transaction.name = "Testwert";
    transaction.description = "Das ist ein Test";
    transaction.type = "expense";
    transaction.value = "111.11";
    transaction.user = await helper
      .getRepo(User)
      .findOneOrFail({ email: "dev@fwe.de" });
    const savedTransaction = await helper
      .getRepo(Transaction)
      .save(transaction);
    const authToken = await helper.loginUser("dev@fwe.de");
    request(helper.app)
      .get("/api/transaction")
      .set("Content-Type", "application/json")
      .set("Accept", "application/json")
      .set("Authorization", authToken)
      .expect(200)
      .end((err, res) => {
        if (err) throw err;
        expect(res.body.data.length).toBe(1);
        expect(res.body.data[0].name).toBe(savedTransaction.name);
        expect(res.body.data[0].id).toBe(savedTransaction.id);
        expect(res.body.data[0].description).toBe(savedTransaction.description);
        expect(res.body.data[0].type).toBe(savedTransaction.type);
        expect(res.body.data[0].value).toBe(savedTransaction.value);
        done();
      });
  });

  it("should show no transactions if the user is not logged in", async (done) => {
    await helper.resetDatabase();
    await helper.loadFixtures();
    request(helper.app)
      .get("/api/transaction")
      .set("Content-Type", "application/json")
      .set("Accept", "application/json")
      .expect(401)
      .end((err) => {
        if (err) throw err;
        done();
      });
  });

  it("it should be able to create a new transaction without tags", async (done) => {
    await helper.resetDatabase();
    await helper.loadFixtures();
    const authToken = await helper.loginUser("dev@fwe.de");
    request(helper.app)
      .post("/api/transaction")
      .send({
        description: "Das ist ein Test",
        name: "Testwert",
        type: "expense",
        value: "293.99",
      })
      .set("Content-Type", "application/json")
      .set("Accept", "application/json")
      .set("Authorization", authToken)
      .end((err, res) => {
        if (err) throw err;
        expect(res.body.data.name).toBe("Testwert");
        expect(res.body.data.description).toBe("Das ist ein Test");
        expect(res.body.data.value).toBe("293.99");
        expect(res.body.data.type).toBe("expense");
        expect(res.body.data.id).toBeDefined();
        expect(res.body.data.createdAt).toBeDefined();
        expect(res.body.data.updatedAt).toBeDefined();
        expect(res.body.data.updatedAt).toBe(res.body.data.createdAt);
        expect(res.body.data.tags).toBeDefined();
        done();
      });
  });

  it("it should be able to create a new transaction with tags", async (done) => {
    await helper.resetDatabase();
    await helper.loadFixtures();
    const authToken = await helper.loginUser("dev@fwe.de");
    request(helper.app)
      .post("/api/transaction")
      .send({
        description: "Das ist ein Test",
        name: "Testwert",
        tags: [{ label: "Testtag" }],
        type: "expense",
        value: "293.99",
      })
      .set("Content-Type", "application/json")
      .set("Accept", "application/json")
      .set("Authorization", authToken)
      .end((err, res) => {
        if (err) throw err;
        expect(res.body.data.name).toBe("Testwert");
        expect(res.body.data.description).toBe("Das ist ein Test");
        expect(res.body.data.value).toBe("293.99");
        expect(res.body.data.type).toBe("expense");
        expect(res.body.data.id).toBeDefined();
        expect(res.body.data.createdAt).toBeDefined();
        expect(res.body.data.updatedAt).toBeDefined();
        expect(res.body.data.updatedAt).toBe(res.body.data.createdAt);
        expect(res.body.data.tags).toBeDefined();
        expect(res.body.data.tags[0].label).toBe("Testtag");
        done();
      });
  });

  it("it should be able to delete a transaction", async (done) => {
    await helper.resetDatabase();
    await helper.loadFixtures();
    const transaction = new Transaction();
    transaction.name = "Testwert";
    transaction.description = "Das ist ein Test";
    transaction.type = "expense";
    transaction.value = "111.11";
    const savedTransaction = await helper
      .getRepo(Transaction)
      .save(transaction);
    const authToken = await helper.loginUser("dev@fwe.de");
    request(helper.app)
      .delete(`/api/transaction/${savedTransaction.id}`)
      .set("Content-Type", "application/json")
      .set("Accept", "application/json")
      .set("Authorization", authToken)
      .end(async (err) => {
        if (err) throw err;
        const [, transactionCount] = await helper
          .getRepo(Transaction)
          .findAndCount();
        expect(transactionCount).toBe(0);
        done();
      });
  });

  it("it should be able to update a transaction", async (done) => {
    await helper.resetDatabase();
    await helper.loadFixtures();
    const transaction = new Transaction();
    transaction.name = "Testwert";
    transaction.description = "Das ist ein Test";
    transaction.type = "expense";
    transaction.value = "111.11";
    const savedTransaction = await helper
      .getRepo(Transaction)
      .save(transaction);
    const authToken = await helper.loginUser("dev@fwe.de");
    request(helper.app)
      .patch(`/api/transaction/${savedTransaction.id}`)
      .send({
        description: "Edited Description",
        name: "Edited Name",
        type: "income",
        value: "10.10",
      })
      .set("Content-Type", "application/json")
      .set("Accept", "application/json")
      .set("Authorization", authToken)
      .end(async (err, res) => {
        if (err) throw err;
        expect(res.body.data.name).toBe("Edited Name");
        expect(res.body.data.description).toBe("Edited Description");
        expect(res.body.data.value).toBe("10.10");
        expect(res.body.data.type).toBe("income");
        done();
      });
  });

  it("it should be able to get a single transaction", async (done) => {
    await helper.resetDatabase();
    await helper.loadFixtures();
    const transaction = new Transaction();
    transaction.name = "Testwert";
    transaction.description = "Das ist ein Test";
    transaction.type = "expense";
    transaction.value = "111.11";
    const savedTransaction = await helper
      .getRepo(Transaction)
      .save(transaction);
    const authToken = await helper.loginUser("dev@fwe.de");
    request(helper.app)
      .get(`/api/transaction/${savedTransaction.id}`)
      .set("Content-Type", "application/json")
      .set("Accept", "application/json")
      .set("Authorization", authToken)
      .end(async (err, res) => {
        if (err) throw err;
        expect(res.body.data.id).toBe(savedTransaction.id);
        expect(res.body.data.name).toBe(savedTransaction.name);
        expect(res.body.data.description).toBe(savedTransaction.description);
        expect(res.body.data.value).toBe(savedTransaction.value);
        expect(res.body.data.type).toBe(savedTransaction.type);
        expect(res.body.data.createdAt).toBeDefined();
        expect(res.body.data.updatedAt).toBeDefined();
        done();
      });
  });
});
