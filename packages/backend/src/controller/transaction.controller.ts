import { validate } from "class-validator";
import { Request, Response } from "express";
import {
  EntityManager,
  getConnection,
  getRepository,
  Repository,
} from "typeorm";
import { Transaction } from "../entity/Transaction";
import { Tag } from "./../entity/Tag";
import { User } from "./../entity/User";

export class TransactionController {
  /**
   * Get all transactions
   * TODO: Error handling
   *
   * @param {Request} req Request
   * @param {Response} res Response
   * @param {NextFunction} next NextFunction
   * @returns {Promise<void>}
   */
  public static async getAllTransactions(req: Request, res: Response) {
    const transactionRepository: Repository<Transaction> = getRepository(
      Transaction
    );

    const token = req.token;
    if (token === null) {
      res.status(400).send({ message: "no-token" });
      return;
    }
    const transactions: Transaction[] = await transactionRepository
      .createQueryBuilder("transaction")
      .leftJoinAndSelect("transaction.tags", "tags")
      .leftJoin("transaction.user", "user")
      .where("user.id=:id", { id: req.token!.id })
      .getMany();
    res.send({ data: transactions });
  }

  /**
   * Create a transaction
   * Required body attributes: name, value, type
   * Optional body attributes: description
   * - Validate if all necessary fields are added
   * Return 400 if something is incorrect
   * - Create transaction
   * TODO: Error handling if create transaction fails
   *
   * @param {Request} req Request
   * @param {Response} res Response
   * @param {NextFunction} next NextFunction
   * @returns {Promise<void>}
   */
  public static async createTransaction(req: Request, res: Response) {
    const { name, value, description, type, tags } = req.body;
    const typedTags = tags as Tag[];
    let dbTags: Tag[] = [];

    /** We are doing a upsert via code here */
    const checkExisting = async (tagName: string, manager: EntityManager) => {
      const existingTag = await manager.getRepository(Tag).findOne({
        where: { label: tagName },
      });
      const enTag = new Tag();
      enTag.label = tagName;

      return existingTag || manager.save(enTag);
    };

    const connection = getConnection();
    const queryRunner = connection.createQueryRunner();

    await queryRunner.startTransaction();

    try {
      if (tags) {
        const promiseTags = await Promise.all(
          typedTags.reduce<Promise<Tag | undefined>[]>((prev, tag) => {
            if (tag.id) {
              prev.push(queryRunner.manager.getRepository(Tag).findOne(tag.id));
              return prev;
            }
            if (tag.label) {
              prev.push(checkExisting(tag.label, queryRunner.manager));
              return prev;
            }
            return prev;
          }, [])
        );
        dbTags = promiseTags.filter((tag) => tag !== undefined) as Tag[];
      }
      /** done with cascading tags via code */

      const currentUser = await queryRunner.manager
        .getRepository(User)
        .findOneOrFail(req.token!.id);
      const transaction = new Transaction();
      transaction.name = name;
      transaction.value = value;
      transaction.description = description;
      transaction.type = type;
      transaction.user = currentUser || null;
      transaction.tags = dbTags;

      const errors = await validate(transaction);
      if (errors.length > 0) {
        res.status(400).send({ errors });
        return;
      }

      const transactionRepository: Repository<Transaction> = queryRunner.manager.getRepository(
        Transaction
      );
      const createdTransaction: Transaction = await transactionRepository.save(
        transaction
      );
      await queryRunner.commitTransaction();

      res.send({ data: createdTransaction });
    } catch (e) {
      await queryRunner.rollbackTransaction();
      res.status(500).send(JSON.stringify(e));
    } finally {
      await queryRunner.release();
    }
  }

  /**
   * Get a transaction by id
   * Required params: transactionId
   * Return 404 if transaction not found
   *
   * @param {Request} req Request
   * @param {Response} res Response
   * @param {NextFunction} next NextFunction
   * @returns {Promise<void>}
   */
  public static async getSingleTransaction(req: Request, res: Response) {
    const transactionId: string = req.params.transactionId;
    const transactionRepository: Repository<Transaction> = getRepository(
      Transaction
    );

    try {
      const transaction: Transaction = await transactionRepository.findOneOrFail(
        transactionId
      );
      res.send({ data: transaction });
    } catch (error) {
      res.status(404).send({ status: "not_found" });
    }
  }

  /**
   * Delete a transaction by id
   * Required params: transactionId
   * Return 404 if transaction doesn't exist
   * - Delete transaction
   *
   * @param {Request} req Request
   * @param {Response} res Response
   * @param {NextFunction} next NextFunction
   * @returns {Promise<void>}
   */
  public static async deleteTransaction(req: Request, res: Response) {
    const transactionId: string = req.params.transactionId;
    const transactionRepository: Repository<Transaction> = getRepository(
      Transaction
    );

    try {
      const transaction: Transaction = await transactionRepository.findOneOrFail(
        transactionId
      );
      await transactionRepository.remove(transaction);
      res.send({ status: "ok" });
    } catch (error) {
      res.status(404).send({ status: "not_found" });
    }
  }

  /**
   * Update values of a transaction by id
   * Allowed attributes: name, value, description, type
   * Return 404 if transaction not found
   * TODO: Validation of attributes
   *
   * @param {Request} req Request
   * @param {Response} res Response
   * @param {NextFunction} next NextFunction
   * @returns {Promise<void>}
   */
  public static async patchTransaction(req: Request, res: Response) {
    const transactionId: string = req.params.transactionId;
    const { name, value, description, type } = req.body;

    const transactionRepository: Repository<Transaction> = getRepository(
      Transaction
    );

    try {
      let transaction: Transaction = await transactionRepository.findOneOrFail(
        transactionId
      );
      transaction.name = name;
      transaction.value = value;
      transaction.description = description;
      transaction.type = type;

      transaction = await transactionRepository.save(transaction);
      res.send({ data: transaction });
    } catch (error) {
      res.status(404).send({ status: "not_found" });
    }
  }
}
