import { validate } from "class-validator";
import { Request, Response } from "express";
import fetch from "node-fetch";
import { getRepository, Repository } from "typeorm";
import { User } from "../entity/User";
import { Authentication } from "../middleware/authentication";

interface PlaceHolderUser {
  id: number;
  name: string;
  email: string;
}

export class UserController {
  /**
   * Query a list of users from an external api
   * TODO: Error handling if api response fails
   * TODO: Type for api response
   *
   * @param {Request} req Request
   * @param {Response} res Response
   * @param {NextFunction} next NextFunction
   * @returns {Promise<any>}
   */
  public static async getUsersFromExternalApi(
    // tslint:disable-next-line: variable-name
    _req: Request,
    res: Response
  ): Promise<void> {
    const apiResponse = (await fetch(
      "https://jsonplaceholder.typicode.com/users"
    ).then((resp) => resp.json())) as PlaceHolderUser[];
    const users = apiResponse.map((user) => {
      return {
        email: user.email,
        id: user.id,
        name: user.name,
      };
    });
    res.send({ data: users });
  }

  /**
   * Create a new user
   *
   * @param {Request} req Request
   * @param {Response} res Response
   * @param {NextFunction} next NextFunction
   * @returns {Promise<any>}
   */
  public static async createUser(req: Request, res: Response): Promise<void> {
    const { email, name, password } = req.body;
    const userRepository: Repository<User> = getRepository(User);
    // Check if user already exists
    const user = await userRepository.findOne({
      where: {
        email,
      },
    });

    if (user) {
      res.status(400).send({ status: "bad_request" });
      return;
    }

    // Generate hashed password
    const hashedPassword: string = await Authentication.hashPassword(password);

    const newUser = new User();
    newUser.email = email;
    newUser.name = name;
    newUser.password = hashedPassword;

    const errors = await validate(newUser);
    if (errors.length > 0) {
      res.status(400).send({ status: "bad_request" });
      return;
    }

    const createdUser = await userRepository.save(newUser);

    // Make sure to not return the hashed password
    delete createdUser.password;

    res.send({ data: createdUser });
  }

  /**
   * Login a user by creating a valid JWT token
   * Returns 401 if anything fails
   * - Check if user exists
   * - Check if password matches
   * - Generate token
   *
   * @param {Request} req Request
   * @param {Response} res Response
   * @param {NextFunction} next NextFunction
   * @returns {Promise<any>}
   */
  public static async loginUser(req: Request, res: Response): Promise<void> {
    const { email, password } = req.body;
    const userRepository: Repository<User> = getRepository(User);
    // Check if user exists
    const user = await userRepository.findOne({
      select: ["password", "email", "name", "id"],
      where: {
        email,
      },
    });

    // If the user doesn't exist we stop
    if (!user) {
      res.status(401).send({ status: "unauthorized " });
      return;
    }

    const matchingPasswords: boolean = await Authentication.comparePasswordWithHash(
      password,
      user.password
    );
    if (!matchingPasswords) {
      res.status(401).send({ status: "unauthorized" });
      return;
    }

    const token: string = await Authentication.generateToken({
      email: user.email,
      id: user.id,
      name: user.name,
    });

    res.send({ data: token });
  }
}
