import { Request, Response } from "express";
import { getRepository, Repository } from "typeorm";
import { Tag } from "../entity/Tag";
export class TagController {
  /**
   * Get all available tags
   * TODO: Error handling if something fails
   *
   * @param {Request} req Request
   * @param {Response} res Response
   * @param {NextFunction} next NextFunction
   * @returns {Promise<void>}
   */
  public static async getAllTags(_: Request, res: Response): Promise<void> {
    const tagRepository: Repository<Tag> = getRepository(Tag);
    const tags: Tag[] = await tagRepository.find();
    res.send({ data: tags });
  }
}
