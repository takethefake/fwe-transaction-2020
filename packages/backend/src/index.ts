// tslint:disable-next-line: no-var-requires
require("dotenv-safe").config();
import * as bodyParser from "body-parser";
import express from "express";
import "reflect-metadata";
import { authMiddleware } from "./middleware/authentication";
import { globalRouter } from "./router/global.router";
import { createDatabaseConnection } from "./util/createDatabaseConnection";
const port = process.env.PORT;

export const startServer = async () => {
  try {
    const app = express();
    app.use(bodyParser.json());
    app.use(authMiddleware);

    app.use("/api", globalRouter);
    const dbConnection = await createDatabaseConnection();
    const server = app.listen(port, () =>
      console.log(`Server is up at http://localhost:${port}`)
    );
    return { server, dbConnection };
  } catch (e) {
    console.log(e);
    throw e;
  }
};

// tslint:disable-next-line: no-floating-promises
startServer();
