import { NextFunction, Request, Response } from "express";

/**
 * Log the current time
 *
 * @param {Request} req Request
 * @param {Response} res Response
 * @param {NextFunction} next NextFunction
 * @returns {Promise<void>}
 */
export async function logTime(
  // tslint:disable-next-line: variable-name
  _req: Request,
  // tslint:disable-next-line: variable-name
  _res: Response,
  next: NextFunction
): Promise<void> {
  console.log(`Current time: ${new Date()}`);
  next();
}
