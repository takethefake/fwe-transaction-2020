/** Package imports */
import { Router } from "express";
import { TransactionController } from "../controller/transaction.controller";

/** Variables */
export const transactionRouter: Router = Router({ mergeParams: true });

/** Routes */
transactionRouter.get("/", TransactionController.getAllTransactions);
transactionRouter.post("/", TransactionController.createTransaction);
transactionRouter.get(
  "/:transactionId",
  TransactionController.getSingleTransaction
);
transactionRouter.delete(
  "/:transactionId",
  TransactionController.deleteTransaction
);
transactionRouter.patch(
  "/:transactionId",
  TransactionController.patchTransaction
);
