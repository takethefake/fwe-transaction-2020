/** Package imports */
import { Router } from "express";
import { Authentication } from "../middleware/authentication";
import { logTime } from "../middleware/logger";
import { tagRouter } from "./tag.router";
import { transactionRouter } from "./transaction.router";
import { userRouter } from "./user.router";

/** Variables */
export const globalRouter: Router = Router({ mergeParams: true });

/** Routes */
globalRouter.use("/tag", Authentication.verifyAccess, tagRouter);
globalRouter.use(
  "/transaction",
  Authentication.verifyAccess,
  logTime,
  transactionRouter
);
globalRouter.use("/user", userRouter);
