import { IsDecimal, IsString } from "class-validator";
import {
  Column,
  CreateDateColumn,
  Entity,
  JoinTable,
  ManyToMany,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from "typeorm";
import { Tag } from "./Tag";
import { User } from "./User";

// TODO: Add more relevant class-validator decorators
@Entity()
export class Transaction {
  @PrimaryGeneratedColumn("uuid")
  id: string;

  @Column()
  @IsString() // Verify that the name is a strings
  name: string;

  @Column({ type: "text", nullable: true })
  description: string;

  @Column()
  @IsDecimal(
    { decimal_digits: "2", locale: "en-US", force_decimal: true },
    { message: "value is not a valid decimal number with two decimal digits" }
  ) // Verify that the value is decimal
  value: string;

  @Column({
    default: "expense",
  })
  type: "income" | "expense";

  @CreateDateColumn()
  createdAt: string;

  @UpdateDateColumn()
  updatedAt: number;

  @ManyToMany(() => Tag, (tag) => tag.transactions, {
    cascade: true,
    eager: true,
  })
  @JoinTable({ name: "transaction_tags" })
  tags: Tag[];

  @ManyToOne(() => User, (user) => user.transactions)
  user: User;
}
